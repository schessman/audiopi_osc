''' AudioPi OSC interface to Ardour
    Copyright (C) 2019 Samuel S Chessman
    SPDX-License-Identifier: GPL-3.0-or-later
'''
# PYTHONFAULTHANDLER=1 python main.py
#import faulthandler; faulthandler.enable()

from kivy.app import App
from kivy.lang import Builder
from kivy.logger import Logger

# Local libraries
from src.osc import ArdourCallbacks, ArdourConnect
from src.components import MixerStrip

# settings label
connection_section = 'Connection'

# kv defines the screens
kv = '''
BoxLayout:
    orientation: 'vertical'
    size_hint: 1,1
    spacing: 1
    padding: 1
    BoxLayout:
        id: transport_labels
        orientation: 'horizontal'
        size_hint_y: None
        size_hint_x: 1
        height: "40dp"
        spacing: 1
        padding: 1
        Button:
            text: 'Settings'
            size: (65,65)
            on_release: app.open_settings()
        Label:
            id: session_name
            pos: 00,10
            text: app.ardour.transport.session_name
            font_size: 18
        Label:
            id: session_time
            pos: 00,50
            text: app.ardour.transport.session_time
            font_size: 18
    BoxLayout:
        id: transport_buttons
        orientation: 'horizontal'
        size_hint_y: None
        height: "40dp"
        spacing: 1
        padding: 1
        Button:
            id: transport_begin
            text: 'Begin'
            on_release: app.ardour.transport.on_begin(self)
        Button:
            id: transport_end
            text: 'End'
            on_release: app.ardour.transport.on_end(self)
        ToggleButton:
            id: transport_play
            text: 'Play'
            on_release: app.ardour.transport.on_play_released(self)
            background_down: 'atlas://textures/myatlas/green_button_pressed'
        ToggleButton:
            id: transport_stop
            text: 'Stop'
            on_release: app.ardour.transport.on_stop_released(self)
            background_down: 'atlas://textures/myatlas/green_button_pressed'
        ToggleButton:
            id: transport_rec
            text: 'Rec'
            on_release: app.ardour.transport.on_rec_released(self)
            background_down: 'atlas://textures/myatlas/red_button_pressed'

    BoxLayout:
        id: mixer_strips
        orientation: 'horizontal'
        size_hint: 1,.7
        spacing: 5
        padding: 5

    BoxLayout:
        id: connection
        orientation: 'horizontal'
        size_hint_y: None
        height: "40dp"
        spacing: 1
        padding: 1
        Label:
            id: label
            text: 'AudioPi'
        Label:
            pos: 20, 10
            text: 'address'
            font_size: 14
        Label:
            pos: 20, 30
            text: app.ardour.ardour_address
            font_size: 14
        Label:
            pos: 40, 10
            text: 'port'
            font_size: 14
        Label:
            pos: 40, 30
            text: str(app.ardour.ardour_port)
            font_size: 14

<MixerStrip>:
    orientation: 'vertical'
    spacing: 2
    padding: 2
    canvas.before:
        Color:
            rgba: .3, .3, .3, .3
        Rectangle:
            pos: self.pos
            size: self.size

<StripButton>:
    size_hint: (None,None)
    size: (65,65)
    background_down: 'atlas://textures/myatlas/green_button_pressed'

<StripLabel>:
    size_hint: (None,None)
    size: (65,65)

<-StripFader@Slider>:
    text_size: 18
    orientation: 'vertical'
    size_hint: (None,None)
    size: (6,600)
    height: 500
    value_track_color: .7, .7, .7, .5
    value_track_width: 10
    pos_hint: {'top': 0}
    cursor_image: 'atlas://textures/myatlas/fader_image'
    canvas:
        # Background
        Color:
            rgb: 1, 1, 1
        BorderImage:
            border: self.border_vertical
            pos: self.center_x - self.background_width / 2, self.y + self.padding
            size: self.background_width, self.height - self.padding * 2
            source: self.background_disabled_vertical if self.disabled else self.background_vertical
        # Slider Track
        Color:
            rgba: self.value_track_color
        Line:
            width: self.value_track_width
            points: self.center_x, self.y + self.padding, self.center_x, self.value_pos[1]
        Color:
            rgb: 1, 1, 1
        Rectangle:
            pos: (self.value_pos[0] - self.cursor_width / 2, self.center_y - self.cursor_height / 2) if self.orientation == 'horizontal' else (self.center_x - self.cursor_width / 2, self.value_pos[1] - self.cursor_height / 2)
            size: self.cursor_size
            source: self.cursor_disabled_image if self.disabled else self.cursor_image

<VerticalMeter>:
    background_width: '18sp'
    size_hint: (None, None)
    text_size: 18
    min: 10
    max: self.height+self.min
    step: 10
    size: (6,600)
    height: 500
    pos_hint: {'top': 0}
    canvas.before:
        Color:
            rgba: 0., 1., 0., .3
        Rectangle:
            pos: self.pos
            size: self.size

'''

# json defines entries to appear in App configuration screen
json = '''
[
    {
        "type": "string",
        "title": "address",
        "desc": "Set IP address of Ardour System",
        "section": "Connection",
        "key": "address"
    },
    {
        "type": "numeric",
        "title": "port number",
        "desc": "Set the port number of Ardour System",
        "section": "Connection",
        "key": "port"
    }
]
'''


class AudioPiApp(App):
    ''' AudioPiApp is the top level widget that runs the mixer.
        It starts the OSC Server sending to the configured UDP xmit addr, port,
        and receiving on the automatically opened UDP recv port on 0.0.0.0.
        xmit configuration data is kept in audiopi.ini json file.
    '''

    def __init__(self, *args, **kwargs):
        """ setup AudioPiApp class """
        super(AudioPiApp,self).__init__(*args, **kwargs)
        self.ardour = ArdourConnect(self)

    def build(self):
        """
        Build and return the root widget.
        """
        self.use_kivy_settings = False

        # apply the saved configuration settings or the defaults
        root = Builder.load_string(kv)
        label = root.ids.label
        label.address = self.config.get(connection_section, 'address')
        self.ardour.set_ardour_address(label.address)
        label.port = int(self.config.get(connection_section, 'port'))
        self.ardour.set_ardour_port(label.port)

        Logger.info ('audiopi: OSC listening on (address, port) '+
            str(self.ardour.osc.getaddress(sock=self.ardour.sock)) )
        self.ardour.send_connect()
        return root


    def ardour_connect(self):
        # Use persistent storage to configure these two properties.
        # reconnect when they change
        self.ardour.ardour_cb.ardour_bind(self.ardour.osc,self.ardour.sock)
        self.ardour.send_connect()

    # android lifecycle
    def on_start(self):
        Logger.info ('audiopi: onstart')
        pass

    def on_stop(self):
        Logger.info ('audiopi: onstop')
        self.ardour.osc.stop_all()
        pass

    def on_pause(self):
        Logger.info ('audiopi: onpause')
        self.ardour.osc.stop_all()
        pass

    def on_resume(self):
        Logger.info ('audiopi: onresume')
        self.ardour_connect()
        pass

    # Settings
    def build_config(self, config):
        """
        Set the default values for the configs sections.
        """
        config.setdefaults(connection_section, {'address': '127.0.0.1', 'port': 3819})

    def build_settings(self, settings):
        """
        Add our custom section to the default configuration object.
        """
        # We use the string defined above for our JSON, but it could also be
        # loaded from a file as follows:
        #     settings.add_json_panel('My Label', self.config, 'settings.json')
        settings.add_json_panel(connection_section, self.config, data=json)

    def on_config_change(self, config, section, key, value):
        """
        Respond to changes in the configuration.
        """
        Logger.info("audiopi: settings.on_config_change: {0}, {1}, {2}, {3}".format(
            config, section, key, value))

        if section == connection_section:
            if key == "address":
                self.root.ids.label.address = value
                self.ardour.set_ardour_address(value)
            elif key == 'port':
                self.root.ids.label.port = int(value)
                self.ardour.set_ardour_port(value)

    def close_settings(self, settings=None):
        """
        The settings panel has been closed.
        """
        Logger.debug("audiopi: settings.close_settings: {0}".format(settings))
        super(AudioPiApp, self).close_settings(settings)
        self.ardour_connect()

if __name__ == '__main__':
    AudioPiApp().run()
