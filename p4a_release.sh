/usr/bin/python -m pythonforandroid.toolchain apk \
  --debug \
  --bootstrap=sdl2 \
  --dist_name audiopi \
  --name AudioPi \
  --version 0.1 \
  --package com.exfavilla.audiopi \
  --android_api 28 \
  --minsdk 22 \
  --private /opt/src/kivy/audiopi/.buildozer/android/app \
  --orientation sensor \
  --release \
  --copy-libs \
  --arch armeabi-v7a \
  --color=always \
  --storage-dir="/opt/src/kivy/audiopi/.buildozer/android/platform/build"

