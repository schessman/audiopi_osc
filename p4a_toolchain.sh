#!/bin/sh
/usr/bin/python -m pythonforandroid.toolchain create \
 --dist_name=audiopi \
 --bootstrap=sdl2 \
 --requirements=python3,kivy,oscpy \
 --arch armeabi-v7a \
 --copy-libs \
 --color=always \
 --storage-dir="/opt/src/kivy/audiopi/.buildozer/android/platform/build"
