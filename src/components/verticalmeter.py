''' AudioPi OSC interface to Ardour
    Copyright (C) 2019 Samuel S Chessman
    SPDX-License-Identifier: GPL-3.0-or-later
'''
from kivy.app import App
from kivy.uix.progressbar import ProgressBar
from kivy.core.text import Label as CoreLabel
from kivy.lang.builder import Builder
from kivy.graphics import Color, Rectangle
from kivy.clock import Clock


class VerticalMeter(ProgressBar):

    def __init__(self, **kwargs):
        super(VerticalMeter, self).__init__(**kwargs)

        # size property defaults are in main.py kv
        self.text_size = 18

        # Create a direct text representation
        self.label = CoreLabel(text="0", font_size=self.text_size)

        # Initialise the texture_size variable
        self.texture_size = None
        #self.cursor_image = ''


        # Refresh the text
        #self.refresh_text()  # this causes segfault
        #self.value_normalized = 0

        # Redraw on init
        self.draw()

    def draw(self):

        with self.canvas: # has __enter__ and __exit__ methods

            # Empty canvas instructions
            self.canvas.clear()

            # Draw background rectangle grey
            Color(0.26, 0.26, 0.26)
            Rectangle(pos=self.pos, size=self.size)

            # Draw meter rectangle green
            Color(0, 1, 0)
            mysize = (self.size[0],self.value)
            Rectangle(pos=self.pos, size=mysize)

            # Center and draw the meter text
            #Color(1, 1, 1, 1) # white
            #Rectangle(texture=self.label.texture,
            #    size=self.texture_size,
            #    pos=(self.pos[0]+(self.size[0]/2 - self.texture_size[0]/2),
            #         self.pos[1]+(self.size[1] + self.texture_size[1])) )

    def refresh_text(self):
        # Render the label
        self.label.refresh()

        # Set the texture size each refresh
        self.texture_size = list(self.label.texture.size)

    def set_value(self, value):
        # Update the progress bar value
        self.value_normalized = value

        # Update textual value and refresh the texture
        self.label.text = str(int(self.value_normalized * 100))
        #self.refresh_text()
 
        # Draw all the elements
        self.draw()


class Main(App):
    ''' test the meter '''

    # draw size,size_hint,pos,pos_hint [5.5, 396.4] [0.01, 0.4] [20, 10] {}
    kv = '''
VerticalMeter:
    size: (5,400)
    size_hint: (None, None)
    text_size: 18
    height: 400
    min: 10
    max: self.height+self.min
    step: 10
    pos: (20,10)
    '''
    # Simple animation to show the vertical meter in action
    def animate(self, dt):
        if self.root.value < self.root.max:
            self.root.set_value(self.root.value + self.root.step)
        else:
            self.root.set_value(self.root.min)

    # Simple layout for easy example
    def build(self):
        container = Builder.load_string(self.kv)

        # Animate the progress bar
        Clock.schedule_interval(self.animate, 0.01)
        return container


if __name__ == '__main__':
    Main().run()
