''' AudioPi OSC interface to Ardour
    Copyright (C) 2019 Samuel S Chessman
    SPDX-License-Identifier: GPL-3.0-or-later
'''
from kivy.logger import Logger
from kivy.properties import StringProperty,NumericProperty,ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.slider import Slider
from kivy.uix.togglebutton import ToggleButton
from .verticalmeter import VerticalMeter

#(strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,sstrip_sid,strip_recenable) = stripreply
# stripreply is list
#Strip type - One of:
#  AT - Audio Track
#  MT - MIDI Track
#  B - Audio Bus
#  MB - MIDI bus
#  AX - Aux bus
#  V - VCA
#  MA - Master
#Strip name
#Number of inputs
#Number of outputs
#Muted
#Soloed
#Ssid (strip number)
#Record enabled - Not Bus

#self.background_down = 'atlas://textures/theme-orange/myatlas/button_pressed'
#self.background_normal = 'atlas://textures/red-lightgrey/myatlas/button_pressed'
#if len(stripreply) == 8:
#    (strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,strip_ssid,strip_recenable) = stripreply
#else: # Bus does not have recenable
#    (strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,strip_ssid) = stripreply
#(strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,sstrip_sid,strip_recenable) = stripreply

def log_msg(msg,values):
    Logger.info(('audiopi: MixerStrip %s ' % msg)+str(values))

# use KV settings
class StripButton(ToggleButton):
    pass

class StripLabel(Label):
    pass

class StripFader(Slider):
    pass

class MixerStrip(BoxLayout):
    ''' vertical mixerstrip widget '''
    fader_sent = False

    def __init__(self, ardour, stripreply, **kwargs):
        super(MixerStrip,self).__init__(**kwargs)

        self.ardour = ardour
        self.strip_type = stripreply[0].decode()
        self.strip_name = stripreply[1].decode()
        self.ssid = int(stripreply[6]) - 1
        self.fader_sent = False

        ''' build MixerStrip Widget '''

        self.stripname = StripLabel(text=self.strip_name,font_size=18,pos_hint={'top':1,'center_x':0.5})
        self.add_widget(self.stripname)  # stripname at top

        # bus and master do not have rec_enable
        if self.strip_type == 'AT':
            self.rec_enable = int(stripreply[7]) == 1 # bool
            self.rec_button = StripButton(text="Rec",
                background_down=\
                'atlas://textures/myatlas/red_button_pressed')
            self.rec_button.bind(on_press=self.on_rec_press)
        else:
            self.rec_button = StripLabel(text='')

        # buttons in 2x3 grid with blank labels for spacing
        buttonbox = GridLayout(cols=2)
        buttonbox.size_hint = (1,.3)
        self.iso_button = StripButton(text="Iso",
            background_down=\
            'atlas://textures/myatlas/red_button_pressed')
        self.iso_button.bind(on_press=self.on_iso_press)
        self.lock_button = StripButton(text="Lock",
            background_down=\
            'atlas://textures/myatlas/red_button_pressed')
        self.lock_button.bind(on_press=self.on_lock_press)
        self.mute_button = StripButton(text="Mute",
            background_down=\
            'atlas://textures/myatlas/orange_button_pressed')
        self.mute_button.bind(on_press=self.on_mute_press)

        buttonbox.add_widget(self.rec_button)    # top left
        buttonbox.add_widget(StripLabel(text=''))# top right
        buttonbox.add_widget(self.iso_button)    # middle left
        buttonbox.add_widget(self.lock_button)   # middle right
        buttonbox.add_widget(self.mute_button)   # bottom left

        # master does not have solo
        if self.strip_type != 'MA':
            self.solo_button = StripButton(text="Solo",
                background_down=\
                'atlas://textures/myatlas/green_button_pressed')
            buttonbox.add_widget(self.solo_button)  # bottom right
            self.solo_button.bind(on_press=self.on_solo_press)
        else:
            self.solo_button = StripLabel(text='')
            buttonbox.add_widget(self.solo_button)# bottom right

        self.add_widget(buttonbox)
        # add a blank label for padding
        self.add_widget(StripLabel(text=''))# above mixer strips

        # mixer is below buttons, four rows, blank labels as padding
        mixerBox = GridLayout(cols=4)
        mixerBox.size_hint = (1,1)
        mixerBox.spacing = 2
        mixerBox.padding = 2

        # slider and meter
        self.fader = StripFader()
        self.fader.bind(value_normalized=self.on_fader_value)
        self.meter = VerticalMeter()
        mixerBox.add_widget(StripLabel(size=(20,65),text=''))# left
        mixerBox.add_widget(self.fader)
        mixerBox.add_widget(StripLabel(text=''))# between strips
        mixerBox.add_widget(self.meter)

        self.add_widget(mixerBox)

        log_msg('MixerStrip __init__', (self.ssid, self.strip_name,self.size) )

    # KV Button callbacks here because ssid is part of the instance
    # button callbacks
    def on_rec_press(self, tb):
        log_msg("rec_enable state changed to %s for " % (tb.state),[self.ssid+1])
        #/strip/recenable ssid rec_st
        rec_st = 1 if tb.state == 'down' else 0
        self.ardour.ardour_sendmsg(b'/strip/recenable',values=[self.ssid+1,rec_st])
        self.rec_enable = (rec_st == 1)

    # iso, lock, mute, solo
    def on_iso_press(self, tb):
        log_msg("iso state changed to %s for " % (tb.state),[self.ssid+1])
        iso_st = 1 if tb.state == 'down' else 0
        self.ardour.ardour_sendmsg(b'/strip/solo_iso',values=[self.ssid+1,iso_st])
        self.iso_enable = (iso_st == 1)

    def on_lock_press(self, tb):
        log_msg("lock state changed to %s for " % (tb.state),[self.ssid+1])
        lock_st = 1 if tb.state == 'down' else 0
        self.ardour.ardour_sendmsg(b'/strip/solo_safe',values=[self.ssid+1,lock_st])
        self.lock_enable = (lock_st == 1)

    def on_mute_press(self, tb):
        log_msg("mute state changed to %s for " % (tb.state),[self.ssid+1])
        mute_st = 1 if tb.state == 'down' else 0
        self.ardour.ardour_sendmsg(b'/strip/mute',values=[self.ssid+1,mute_st])
        self.mute_enable = (mute_st == 1)

    def on_solo_press(self, tb):
        log_msg("solo state changed to %s for " % (tb.state),[self.ssid+1])
        solo_st = 1 if tb.state == 'down' else 0
        self.ardour.ardour_sendmsg(b'/strip/solo',values=[self.ssid+1,solo_st])
        self.solo_enable = (solo_st == 1)

    def on_fader_value(self, instance, value):
        if self.fader_sent:
            self.fader_sent = False
            self.ardour.ardour_sendmsg(b'/strip/fader',values=[self.ssid+1,value])
            #log_msg("fader value not sent to %s for " % str(value),[self.ssid+1])
        else:
            self.fader_sent = True
            if instance.value_normalized != value:
                #log_msg("fader value changed from %s to %s for " % (str(instance.value_normalized),str(value)),[self.ssid+1])
                self.ardour.ardour_sendmsg(b'/strip/fader',values=[self.ssid+1,value])
