''' AudioPi OSC interface to Ardour
    Copyright (C) 2019 Samuel S Chessman
    SPDX-License-Identifier: GPL-3.0-or-later
'''
_all__ = ('ArdourCallbacks','ArdourConnect')

from enum import Enum

from kivy.logger import Logger
from kivy.properties import StringProperty,NumericProperty,ListProperty
from kivy.event import EventDispatcher

# Import OSC
from src.oscpy import OSCThreadServer, send_message
# Import MixerStrip
from src.components import MixerStrip

# from oscpy.parser import format_message, read_message

# ''' Kivy AudioPi OSC Controller '''
#
#    ''' AudioPiMixer(Widget) is the main widget for AudioPi.
#        It handles touch events for the top level controls:
#        Settings, Rewind, Loop, Play, Stop, Rec, SkipToEnd.
#    '''
# player2 = ObjectProperty(None)
def log_msg(msg,values):
    Logger.info(('audiopi: %s ' % msg)+str(values))

def log_cb(msg,values):
    Logger.info(('audiopi: cb %s ' % msg)+str(values))

class StripType(Enum):
    AUDIOTRACK = 1
    MIDITRACK = 2
    AUDIOBUS = 4
    MIDIBUS = 8
    VCA = 16
    MASTER =  32
    MONITOR =  64
    AUX =  128
    SELECTED =  256
    HIDDEN =  512
    USEGROUP =  1024

class Feedback(Enum):
    BUTTONSTATUS = 1
    STRIPCONTROL = 2
    SSIDPATH = 4
    HEARTBEAT = 8
    MASTERSECTION = 16
    BARBEAT = 32
    TIMECODE = 64
    METERDB = 128
    METER16BIT = 256
    SIGNALPRESENT = 512
    POSITIONSAMPLE = 1024
    POSITIONTIME = 2048
    SELECTCHANNEL = 8192
    SLASHREPLY = 16384

## Module Level call back support
# Singleton Pattern
# works in Python 2 & 3
class _Singleton(type):
    """ A metaclass that creates a Singleton base class when called. """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Singleton(_Singleton('SingletonMeta', (object,), {})): pass

''' Singleton classes use kivy properties to interface with OSC callback
    created with app reference to access kv root attributes
    created with ArdourConnect reference to access send_message
    Mixer
    Transport
'''
class Mixer(Singleton,EventDispatcher):
    strips = ListProperty([])

    def __init__(self, app, ardour, **kwargs):
        super(Mixer, self).__init__(**kwargs)
        self.app = app
        self.ardour = ardour
        #log_msg("mixer init",str(self.app))

    # stripreply is list
    #Strip type - One of:
    #  AT - Audio Track
    #  MT - MIDI Track
    #  B - Audio Bus
    #  MB - MIDI bus
    #  AX - Aux bus
    #  V - VCA
    #Strip name
    #Number of inputs
    #Number of outputs
    #Muted
    #Soloed
    #Ssid (strip number)
    #Record enabled
    def del_strips(self):
        ''' remove, free the MixerStrips and empty the list.  '''
        #log_msg ('del_strip len %d' % len(self.strips), self.strips)
        for i,w in enumerate(self.strips):
            #log_msg ('del_strip %d' % i, w)
            self.app.root.ids.mixer_strips.remove_widget(w)
        self.strips.clear()

    def add_strip(self, stripreply):
        if len(stripreply) < 7: # end_route_list
            return
        if len(stripreply) == 8:
            (strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,strip_ssid,strip_recenable) = stripreply
        else: # Bus does not have recenable
            (strip_type,strip_name,strip_ins,strip_outs,strip_muted,strip_soloed,strip_ssid) = stripreply
        # make ssid zero relative
        ssid = int(strip_ssid) - 1
        # add a mixerstrip
        mixerstrip = MixerStrip(self.ardour,stripreply)
        self.app.root.ids.mixer_strips.add_widget(mixerstrip)
        self.strips.insert(ssid, mixerstrip )

class Transport(Singleton,EventDispatcher):
    session_name = StringProperty('')
    session_time = StringProperty('00:00:00.00')
    transportplay = StringProperty('normal')
    transportstop = StringProperty('down')
    rec_enable = StringProperty('normal')

    def __init__(self, app, ardour, **kwargs):
        super(Transport, self).__init__(**kwargs)
        self.app = app
        self.ardour = ardour
        #log_msg("transport init",str(self.app))

    # helpers for OSC callbacks
    def on_session_name(self,instance, value):
        log_msg("session_name changed to %s" % value,[])

    def set_session_time(self,time):
        self.session_time = time.decode()
        log_msg("session_time set to %s" % time,[])

    #def on_session_time(self,instance, value):
    #    log_msg("session_time changed to %s" % value,[])

    #def on_transportplay(self,instance, value):
    #    log_msg("transportplay property changed to %s" % value,[])

    #def on_transportstop(self, instance, value):
    #    log_msg("transportstop property changed to %s" % value,[])

    #def on_recenable(self, instance, value):
    #    log_msg("rec_enable property changed to %s" % value,[])

    # helpers for kv state access from tb presses

    def on_begin(self, b):
        log_msg("begin released %s" % b.state,[])
        self.ardour.ardour_sendmsg(b'/goto_start')

    def on_end(self, b):
        log_msg("end released %s" % b.state,[])
        self.ardour.ardour_sendmsg(b'/goto_end')

    def on_play_released(self, tb):
        log_msg("transportplay released changed to %s" % tb.state,[])
        if tb.state == 'down':
            self.ardour.ardour_sendmsg(b'/transport_play')
        else:
            self.app.root.ids.transport_play.state = 'down'

    def on_rec_released(self, tb):
        log_msg("rec_enable released changed to %s" % tb.state,[])
        self.ardour.ardour_sendmsg(b'/rec_enable_toggle',values=[1,])

    def on_stop_released(self, tb):
        log_msg("transportstop released changed to %s" % tb.state,[])
        if tb.state == 'down':
            self.ardour.ardour_sendmsg(b'/transport_stop')
        else:
            self.app.root.ids.transport_stop.state = 'down'


class ArdourCallbacks(object):
    def __init__(self, ardour):
        self.ardour = ardour

    def bank_down(*values): log_cb('bank_down',values)
    def bank_up(*values): log_cb('bank_up',values)
    def cancel_all_solos(*values): log_cb('cancel_all_solos',values)
    def ffwd(*values): log_cb('ffwd',values)
    def loop_toggle(*values): log_cb('loop_toggle',values)
    def manual(*values): log_cb('manual',values)
    def master_fader(*values): pass #log_cb('master_fader',values)
    def master_gain(*values): log_cb('master_gain',values)
    def master_meter(*values):
        pass # log_cb('master_meter',values)
    def master_mute(*values): log_cb('master_mute',values)
    def master_name(*values): pass #log_cb('master_name',values)
    def master_pan_stereo_position(*values): log_cb('master_pan_stereo_position',values)
    def master_signal(*values): log_cb('master_signal',values)
    def master_trimdb(*values): log_cb('master_trimdb',values)
    def monitor_name(*values): log_cb('monitor_name',values)
    def position_smpte(*values):
        #log_cb('position_smpte',values)
        Transport().app.root.ids.session_time.text = values[0].decode()

    def position_time(*values):
        log_cb('position_time',values)
        #Transport().app.root.ids.session_time.text = values[0].decode()

    def rec_enable_toggle(*values):
        log_cb('rec_enable_toggle',values)
        Transport().app.root.ids.transport_rec.state = 'down' if \
            values[0] == 1 else 'normal'

    def record_tally(*values): log_cb('record_tally',values)

    # reply returns
    #Strip type - One of:
    #  AT - Audio Track
    #  MT - MIDI Track
    #  B - Audio Bus
    #  MB - MIDI bus
    #  AX - Aux bus
    #  V - VCA
    #Strip name
    #Number of inputs
    #Number of outputs
    #Muted
    #Soloed
    #Ssid (strip number)
    #Record enabled
    #A bus will not have a record enable
    #A bus message will have one less parameter than a track.
    #It is the controllers responsibility to deal with this.
    #
    #one final message is sent with:
    #The text end_route_list
    #The session frame rate
    #The last frame number of the session
    #Monitor section present
    def reply(*values):
        log_cb('reply',values)
        Mixer().add_strip(values)

    def rewind(*values): log_cb('rewind',values)
    def select_comment(*values): log_cb('select_comment',values)
    def select_comp_enable(*values): log_cb('select_comp_enable',values)
    def select_comp_makeup(*values): log_cb('select_comp_makeup',values)
    def select_comp_mode(*values): log_cb('select_comp_mode',values)
    def select_comp_mode_name(*values): log_cb('select_comp_mode_name',values)
    def select_comp_speed(*values): log_cb('select_comp_speed',values)
    def select_comp_speed_name(*values): log_cb('select_comp_speed_name',values)
    def select_comp_threshold(*values): log_cb('select_comp_threshold',values)
    def select_expand(*values): log_cb('select_expand',values)
    def select_fader(*values): log_cb('select_fader',values)
    def select_fader_automation(*values): log_cb('select_fader_automation',values)
    def select_fader_automation_name(*values): log_cb('select_fader_automation_name',values)
    def select_gain(*values): log_cb('select_gain',values)
    def select_gain_automation(*values): log_cb('select_gain_automation',values)
    def select_gain_automation_name(*values): log_cb('select_gain_automation_name',values)
    def select_meter(*values): log_cb('select_meter',values)
    def select_monitor_disk(*values): log_cb('select_monitor_disk',values)
    def select_monitor_input(*values): log_cb('select_monitor_input',values)
    def select_mute(*values): log_cb('select_mute',values)
    def select_n_inputs(*values): log_cb('select_n_inputs',values)
    def select_n_outputs(*values): log_cb('select_n_outputs',values)
    def select_name(*values): log_cb('select_name',values)
    def select_pan_elevation_position(*values): log_cb('select_pan_elevation_position',values)
    def select_pan_frontback_position(*values): log_cb('select_pan_frontback_position',values)
    def select_pan_lfe_control(*values): log_cb('select_pan_lfe_control',values)
    def select_pan_stereo_position(*values): log_cb('select_pan_stereo_position',values)
    def select_pan_stereo_width(*values): log_cb('select_pan_stereo_width',values)
    def select_plugin_name(*values): log_cb('select_plugin_name',values)
    def select_plugin_parameter(*values): log_cb('select_plugin_parameter',values)
    def select_plugin_parameter_name(*values): log_cb('select_plugin_parameter_name',values)
    def select_polarity(*values): log_cb('select_polarity',values)
    def select_recenable(*values): log_cb('select_recenable',values)
    def select_record_safe(*values): log_cb('select_record_safe',values)
    def select_select(*values): log_cb('select_select',values)
    def select_signal(*values): log_cb('select_signal',values)
    def select_solo(*values): log_cb('select_solo',values)
    def select_solo_iso(*values): log_cb('select_solo_iso',values)
    def select_solo_safe(*values): log_cb('select_solo_safe',values)
    def select_trimdB(*values): log_cb('select_trimdB',values)

    def session_name(*values):
        log_cb('session_name',values)
        Transport().session_name = values[0].decode()

    def strip_expand(*values): log_cb('strip_expand',values)
    def strip_fader(*values):
        ssid = values[0] - 1
        m = Mixer()
        if len(m.strips) > ssid:
            m.strips[ssid].fader_sent = False if m.strips[ssid].fader_sent else True
            m.strips[ssid].fader.value_normalized = values[1]
            #log_cb('strip_fader',values)

    def strip_fader_automation(*values): log_cb('strip_fader_automation',values)
    def strip_fader_automation_name(*values): log_cb('strip_fader_automation_name',values)
    def strip_gain(*values): log_cb('strip_gain',values)
    def strip_gain_automation(*values): log_cb('strip_gain_automation',values)
    def strip_gain_automation_name(*values): log_cb('strip_gain_automation_name',values)
    def strip_gui_select(*values): log_cb('strip_gui_select',values)
    def strip_list(*values): log_cb('strip_list',values)
    def strip_meter(*values): 
        # log_cb('strip_meter',values)
        ssid = values[0] - 1
        if len(Mixer().strips) > ssid:
            Mixer().strips[ssid].meter.set_value(values[1])
    def strip_monitor_disk(*values): log_cb('strip_monitor_disk',values)
    def strip_monitor_input(*values): log_cb('strip_monitor_input',values)
    def strip_mute(*values):
        log_cb('strip_mute',values)
        ssid = values[0] - 1
        mute_st = values[1]
        if len(Mixer().strips) > ssid:
            log_msg('strip mute tb state %s' % Mixer().strips[ssid].mute_button.state,[])
            Mixer().strips[ssid].mute_button.state = 'down' if mute_st == 1 else 'normal'
    def strip_name(*values):
        # if strip_name has changed, force reconnect
        ssid = values[0] - 1
        st_name = values[1].decode()
        log_msg('strip %d name %s' % (values[0],st_name),[])
        m = Mixer()
        if len(m.strips) > ssid and \
          m.strips[ssid].strip_name != st_name:
          if st_name[0] not in '-0123456789.':
            log_cb('strip_name changed from %s to %s'%(m.strips[ssid].strip_name,st_name),values)
            m.ardour.send_connect()

    def strip_pan_stereo_position(*values): log_cb('strip_pan_stereo_position',values)
    def strip_recenable(*values):
        log_cb('strip_recenable',values)
        ssid = values[0] - 1
        rec_st = values[1]
        if len(Mixer().strips) > ssid:
            log_msg('strip_recenable tb state %s' % Mixer().strips[ssid].rec_button.state,[])
            Mixer().strips[ssid].rec_button.state = 'down' if rec_st == 1 else 'normal'
    def strip_record_safe(*values): log_cb('strip_record_safe',values)
    def strip_select(*values): log_cb('strip_select',values)
    def strip_signal(*values): log_cb('strip_signal',values)
    def strip_solo(*values):
        log_cb('strip_solo',values)
        ssid = values[0] - 1
        solo_st = values[1]
        if len(Mixer().strips) > ssid:
            if Mixer().strips[ssid].strip_type != 'MA':
                log_msg('strip solo tb state %s' % Mixer().strips[ssid].solo_button.state,[])
                Mixer().strips[ssid].solo_button.state = 'down' if solo_st == 1 else 'normal'
    def strip_solo_iso(*values):
        log_cb('strip_solo_iso',values)
        ssid = values[0] - 1
        iso_st = values[1]
        if len(Mixer().strips) > ssid:
            log_msg('strip iso tb state %s' % Mixer().strips[ssid].iso_button.state,[])
            Mixer().strips[ssid].iso_button.state = 'down' if iso_st == 1 else 'normal'
    def strip_solo_safe(*values):
        log_cb('strip_solo_safe',values)
        ssid = values[0] - 1
        lock_st = values[1]
        if len(Mixer().strips) > ssid:
            log_msg('strip lock tb state %s' % Mixer().strips[ssid].lock_button.state,[])
            Mixer().strips[ssid].lock_button.state = 'down' if lock_st == 1 else 'normal'
    def strip_trimdb(*values): log_cb('strip_trimdb',values)
    def transport_play(*values):
        log_cb('transport_play',values)
        Transport().app.root.ids.transport_play.state = 'down' if \
            values[0] == 1 else 'normal'
    def transport_stop(*values):
        log_cb('transport_stop',values)
        Transport().app.root.ids.transport_stop.state = 'down' if \
            values[0] == 1 else 'normal'

    ''' dispatch dictionary to map OSC addresses to callback functions '''
    """
    """
    ardour_osc_addresses = {

    b'/Manual':master_gain,
    b'/bank_down':bank_down,
    b'/bank_up':bank_up,
    b'/cancel_all_solos':cancel_all_solos,
    b'/ffwd':ffwd,
    b'/loop_toggle':loop_toggle,
    b'/master/fader':master_fader,
    b'/master/gain':master_gain,
    b'/master/meter':master_meter,
    b'/master/mute':master_mute,
    b'/master/name':master_name,
    b'/master/pan_stereo_position':master_pan_stereo_position,
    b'/master/signal':master_signal,
    b'/master/trimdB':master_trimdb,
    b'/monitor/name':monitor_name,
    b'/position/smpte':position_smpte,
    b'/position/time':position_time,
    b'/rec_enable_toggle':rec_enable_toggle,
    b'/record_tally':record_tally,
    b'/reply':reply,
    b'/rewind':rewind,
    b'/select/comment':select_comment,
    b'/select/comp_enable':select_comp_enable,
    b'/select/comp_makeup':select_comp_makeup,
    b'/select/comp_mode':select_comp_mode,
    b'/select/comp_mode_name':select_comp_mode_name,
    b'/select/comp_speed':select_comp_speed,
    b'/select/comp_speed_name':select_comp_speed_name,
    b'/select/comp_threshold':select_comp_threshold,
    b'/select/expand':select_expand,
    b'/select/fader':select_fader,
    b'/select/fader/automation':select_fader_automation,
    b'/select/fader/automation_name':select_fader_automation_name,
    b'/select/gain':select_gain,
    b'/select/gain/automation':select_gain_automation,
    b'/select/gain/automation_name':select_gain_automation_name,
    b'/select/meter':select_meter,
    b'/select/monitor_disk':select_monitor_disk,
    b'/select/monitor_input':select_monitor_input,
    b'/select/mute':select_mute,
    b'/select/n_inputs':select_n_inputs,
    b'/select/n_outputs':select_n_outputs,
    b'/select/name':select_name,
    b'/select/pan_elevation_position':select_pan_elevation_position,
    b'/select/pan_frontback_position':select_pan_frontback_position,
    b'/select/pan_lfe_control':select_pan_lfe_control,
    b'/select/pan_stereo_position':select_pan_stereo_position,
    b'/select/pan_stereo_width':select_pan_stereo_width,
    b'/select/plugin/name':select_plugin_name,
    b'/select/plugin/parameter':select_plugin_parameter,
    b'/select/plugin/parameter/name':select_plugin_parameter_name,
    b'/select/polarity':select_polarity,
    b'/select/recenable':select_recenable,
    b'/select/record_safe':select_record_safe,
    b'/select/select':select_select,
    b'/select/signal':select_signal,
    b'/select/solo':select_solo,
    b'/select/solo_iso':select_solo_iso,
    b'/select/solo_safe':select_solo_safe,
    b'/select/trimdB':select_trimdB,
    b'/session_name':session_name,
    b'/strip/expand':strip_expand,
    b'/strip/fader':strip_fader,
    b'/strip/fader/automation':strip_fader_automation,
    b'/strip/fader/automation_name':strip_fader_automation_name,
    b'/strip/gain':strip_gain,
    b'/strip/gain/automation':strip_gain_automation,
    b'/strip/gain/automation_name':strip_gain_automation_name,
    b'/strip/gui_select':strip_gui_select,
    b'/strip/list':strip_list,
    b'/strip/meter':strip_meter,
    b'/strip/monitor_disk':strip_monitor_disk,
    b'/strip/monitor_input':strip_monitor_input,
    b'/strip/mute':strip_mute,
    b'/strip/name':strip_name,
    b'/strip/pan_stereo_position':strip_pan_stereo_position,
    b'/strip/recenable':strip_recenable,
    b'/strip/record_safe':strip_record_safe,
    b'/strip/select':strip_select,
    b'/strip/signal':strip_signal,
    b'/strip/solo':strip_solo,
    b'/strip/solo_iso':strip_solo_iso,
    b'/strip/solo_safe':strip_solo_safe,
    b'/strip/trimdB':strip_trimdb,
    b'/transport_play':transport_play,
    b'/transport_stop':transport_stop

    }

    def ardour_bind(self,osc,sock):
        for addr,callback in self.ardour_osc_addresses.items():
            osc.bind(addr,callback,sock)

class ArdourConnect(EventDispatcher):
    """ created with reference to App so app.root.ids from KV file can be used.
    provides connection variables
      ardour_address: StringProperty()
      ardour_port: NumericProperty()
      osc: OSCThreadServer()
      sock: Socket() """
    ardour_address = StringProperty('127.0.0.1')
    ardour_port = NumericProperty(3819)

    def __init__(self, app, *args, **kwargs):
        super(ArdourConnect,self).__init__(*args, **kwargs)
        # Setup OSC
        self.osc = OSCThreadServer(default_handler=self.unconfigured_address)
        # Listen for replies
        self.sock = self.osc.listen('0.0.0.0')
        # handle the replies
        self.ardour_cb = ArdourCallbacks(self)
        self.ardour_cb.ardour_bind(self.osc,self.sock)
        # work around callback not instance method
        # keep App reference to access KV app.root attribute
        self.app = app
        self.transport = Transport(app,self)
        self.mixer = Mixer(app,self)
        # log_msg('ArdourConnect app ',str(self.app) )


    def unconfigured_address(*values):
        log_cb('unconfigured_address',values)
    # ardour_address
    def set_ardour_address(self, addr):
        self.ardour_address = addr
        log_msg('sending UDP to address',(self.ardour_address,self.ardour_port) )

    def on_ardour_address(self, instance, value):
        log_msg("ardour_address changed to %s" % value,[])

    # ardour_port
    def set_ardour_port(self, port):
        self.ardour_port = port
        log_msg('sending UDP to port',(self.ardour_address,self.ardour_port) )

    def on_ardour_port(self, instance, value):
        log_msg("ardour_port changed to %d" % value,[])

    # prime the pump
    def send_connect(self):
        self.mixer.del_strips()
        self.send_strip_list()
        self.send_set_surface()
        #self.send_select_expand()

    # ardour_msg for easy access
    def ardour_sendmsg(self, msg,values=[]):
        #log_msg('sending %s to %s:%d ' % (msg,self.ardour_address,self.ardour_port),values)
        send_message(msg,
          values,
          str(self.ardour_address),
          int(self.ardour_port),
          sock=self.sock,
          safer=False)

    # send this to setup feedback
    def send_set_surface(self):
        ''' /set_surface
            bank_size
            strip_types
            feedback
            fadermode #gainmode 0 -> dB 1-> 0.0 -- 1.0
            send_page_size
            plugin_page_size'''
        values = (0, \
            StripType.AUDIOTRACK.value + \
                StripType.AUDIOBUS.value + \
                StripType.MASTER.value + \
                StripType.AUX.value, \
            Feedback.BUTTONSTATUS.value + \
                Feedback.STRIPCONTROL.value + \
                Feedback.MASTERSECTION.value + \
                Feedback.TIMECODE.value + \
                Feedback.METERDB.value + \
                Feedback.SLASHREPLY.value , \
            1, 0, 0)


        self.ardour_sendmsg(b'/set_surface',values)

    # send this to start the OSC data
    def send_strip_list(self):
        self.ardour_sendmsg(b'/strip/list')
        #log_msg('sending /strip/list to ',(self.ardour_address,self.ardour_port) )
        #send_message(b'/strip/list',
        #  [],
        #  str(self.ardour_address),
        #  int(self.ardour_port),
        #  sock=sock,
        #  safer=False)

    def send_select_expand(self):
        self.ardour_sendmsg(b'/select/expand',[1,])
        #log_msg('sending /select/expand to ',(self.ardour_address,self.ardour_port) )
        #send_message(b'/select/expand',
        #  [1,],
        #  str(self.ardour_address),
        #  int(self.ardour_port),
        #  sock=sock,
        #  safer=False)
