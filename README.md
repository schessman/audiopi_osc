# AudioPi
GPL Android App using Kivy and python to control Ardour5 via OSC for live mixing and recording.

## Platforms
This app targets Android 5.1.1 Lollipop (API 22) or greater.  It works on my Nexus 7 (2012) tablet.  This app needs to connect to Ardour5 with OSC enabled running on another system.  I use RPi3 running Alpine Linux.  See https://audiopi.blogspot.com/

## License
### Copyright (C) 2019, Samuel S Chessman
### SPDX-License-Identifier: GPL-3.0-or-later
The sources are found at https://gitlab.com/schessman/audiopi_osc/

## Credits
The oscpy package is a derivative work of kivy oscpy.
The original python sources for kivy are Copyright (C) 2018 Gabriel Pettier & aland licensed with the MIT license. 
    
The original code can be found at https://github.com/kivy/oscpy/

## Files

* LICENSE - License for distribution
* README.md - this file, basic overview and instructions.
* android.txt - andriod apk definitions
* audiopi.ini - persistent settings file
* buildozer.spec - buildozer settings
* data/ - directory tree with png files
* main.py - main App source file with embedded kv.
* p4a\_build.sh
* p4a\_release.sh
* p4a\_toolchain.sh
* src/ - directory tree with the kivy sources
* src/oscpy/ - directory tree with the modified kivy oscpy sources
* textures/ - directory tree with png files
* todo - freeform issues and tasks
