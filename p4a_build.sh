#!/bin/sh
#
/usr/bin/python -m pythonforandroid.toolchain apk \
 --debug \
 --bootstrap=sdl2 \
 --dist_name audiopi \
 --name AudioPi \
 --version 0.1 \
 --package com.exfavilla.audiopi \
 --android_api 28 \
 --minsdk 22 \
 --private /opt/src/kivy/audiopi/.buildozer/android/app \
 --orientation sensor \
 --copy-libs \
 --arch armeabi-v7a \
 --color=always \
  --permission INTERNET \
 --icon textures/red_ardosc_icon_48px.png \
 --presplash=textures/red_ardour_splash_small.png \
 --storage-dir="/opt/src/kivy/audiopi/.buildozer/android/platform/build"
